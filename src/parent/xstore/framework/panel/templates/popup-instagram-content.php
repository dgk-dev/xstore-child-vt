<div class="et_popup-instagram-content text-left">
	<p>
		<label for="old">
			<input type="radio" id="old" name="type" value="female" checked="" data-url="<?php echo esc_url($_POST['old']); ?>">
			<b><?php esc_html_e('Personal (Legacy API)', 'xstore'); ?></b> <?php esc_html_e('(to display user feeds)', 'xstore') ?>
		</label>
	</p>
	<p style="pointer-events: none; opacity: 0.5;">
		<label for="personal">
			<input type="radio" id="personal" name="type" value="female" data-url="<?php echo esc_url($_POST['personal']); ?>">
			<b><?php esc_html_e('Personal', 'xstore'); ?></b> <?php esc_html_e('(Instagram Basic Display API)', 'xstore') ?>
		</label>
		<span class="et-help-content"><?php esc_html_e('Suspended till 29 June 2020 the future theme update', 'xstore') ?></span>
	</p>
	<p style="pointer-events: none; opacity: 0.5;">
		<label for="business">
			<input type="radio" id="business" name="type" value="other" data-url="<?php echo esc_url($_POST['business']); ?>">
			<b><?php esc_html_e('Business','xstore'); ?></b> <?php esc_html_e('(Graph API) ', 'xstore'); ?>
		</label>
		<span class="et-help-content"><?php esc_html_e('Suspended till 29 June 2020 the future theme update', 'xstore'); ?></span>
	</p>
	<p class="align-center">
		<a class="et-button et-button-green et-get-token et-facebook-corporate" href="<?php echo esc_url($_POST['old']); ?>">
			<span class="dashicons dashicons-instagram"></span>
			<?php esc_html_e('Add account', 'xstore'); ?>
		</a>
	</p>
</div>

<?php
