<?php

// INCLUDES
//agregar pdf de detalle al producto
require_once( trailingslashit( get_stylesheet_directory() ). 'includes/product-uploads.php' );

//cambios generales de la tienda
require_once( trailingslashit( get_stylesheet_directory() ). 'includes/store-changes.php' );

add_action('wp_enqueue_scripts', 'theme_resources');
function theme_resources(){
	etheme_child_styles();

	wp_register_script('vitacen-functions', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
	// wp_localize_script('vitacen-functions', 'vitacenGlobalObject', array(
	// 	'add_cart_button_script' => is_shop() || is_product_category() || is_product_tag(),
	// ));
	wp_enqueue_script('vitacen-functions');
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'vitacen_theme_lang' );
function vitacen_theme_lang() {
    load_child_theme_textdomain( 'xstore', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('xstore-core');
    load_textdomain('xstore-core', get_stylesheet_directory() . '/languages/xstore-core-es_ES.mo');
}

//Whatsapp
add_action('wp_footer','vitacen_add_footer_whatsapp');
function vitacen_add_footer_whatsapp(){
	$tel = "5214432272461 ";

	$url = "https://wa.me/${tel}";
	$img = get_stylesheet_directory_uri().'/img/whatsapp-icon.svg';
	echo "<div id='float-whatsapp' style='position:fixed;bottom:30px;right:30px;z-index:100; text-align:right'>";
	echo " <a href=${url} target='_blank'>";
	echo "<img src='${img}' width=40 height=40 />";
	echo " </a>";
	echo " <div><strong>Escríbenos por Whatsapp</strong></div>";
	echo "</div>";
}

//Google analytics tag
add_action( 'wp_head', 'vitacen_google_analytics', 10 );
function vitacen_google_analytics() {
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-169904854-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-169904854-1');
	</script>
    <?php
}

//Pixel de facebook
add_action( 'wp_head', 'vitacen_facebook_pixel', 10 );
function vitacen_facebook_pixel() {
    ?>
    <!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '618607715679175');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=618607715679175&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
    <?php
}