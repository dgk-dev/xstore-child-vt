<?php

//agregar PDF de detalles
add_action('woocommerce_after_single_product', 'vitacen_download_product_file', 10 );
function vitacen_download_product_file(){
	global $product;
	$fileID = get_post_meta($product->get_ID(), 'vitacen_details_file', true);
	$attachmentURL = wp_get_attachment_url( $fileID );
	
	wp_localize_script('vitacen-functions', 'vitacenProductGlobalObject', array(
		'fileURL' => $attachmentURL,
	));
}

//agregar boton para bajar PDF
function etheme_add_to_cart_catalog_button( $sprintf, $product, $args ) {
	$buttons_html = '';
	$fileID = get_post_meta($product->get_ID(), 'vitacen_details_file', true);
	$attachmentURL = wp_get_attachment_url( $fileID );
	if($attachmentURL){
		$buttons_html .= sprintf( '<a rel="nofollow" href="%s" target="_BLANK" class="button show-product">%s</a>',
			$attachmentURL,
			'Ficha Técnica Diseño'
		);
	}

	$buttons_html .= sprintf( '<a rel="nofollow" href="%s" target="_BLANK" class="button show-product">%s</a>',
		esc_url( 'tel:4432272461' ),
		'Contactar'
	);

	return $buttons_html;
}

//evitar redirección en un solo resultado
add_filter( 'woocommerce_redirect_single_search_result', '__return_false' );

//redirigir a home en single de producto para evitar llegar al detalle
add_action( 'template_redirect', 'vitacen_product_redirection_to_home', 100 );
function vitacen_product_redirection_to_home() {
    if ( ! is_product() ) return; // Only for single product pages.

    wp_redirect( home_url('tienda-vitacascos') ); // redirect home.
    exit();
}