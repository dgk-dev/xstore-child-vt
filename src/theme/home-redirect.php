<?php
/*
Template Name: Redirect To Home
*/
if (have_posts()) {
  while (have_posts()) {
    wp_redirect(get_home_url());
    exit;
  }
}
?>